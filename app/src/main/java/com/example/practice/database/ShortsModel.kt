package com.example.practice.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shorts")
data class ShortsModel(
    @PrimaryKey(autoGenerate = false)
    val id: Long = 0,
    val videoId: String,
    val description: String
)
