package com.example.practice.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [(ShortsModel::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun shortsDao(): ShortsDao

    companion object {
        @Volatile
        private var Instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return Instance?: synchronized(this) {
                Room.databaseBuilder(context, AppDatabase::class.java, "shorts_db")
                    .build().also { Instance = it }
            }
        }
    }

}