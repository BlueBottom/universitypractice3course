package com.example.practice.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ShortsDao {
    @Query("SELECT * FROM shorts")
    fun getShorts(): LiveData<List<ShortsModel>>

    @Query("SELECT * FROM shorts WHERE videoId = :videoId")
    fun getShortsByVideoId(videoId: String): ShortsModel

    @Query("SELECT * FROM shorts WHERE id = :id")
    fun getShortsById(id: Long): LiveData<ShortsModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShort(shortsModel: ShortsModel)

    @Query("DELETE FROM shorts")
    fun delete()

}

