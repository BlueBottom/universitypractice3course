package com.example.practice.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.PlayArrow
import com.example.practice.ui.main.BottomNavItem

object Constants {
    val BottomNavItems = listOf(
        BottomNavItem(
            label = "Shorts",
            icon = Icons.Filled.List,
            route = "shorts"
        ),
        BottomNavItem(
            label = "Carousel",
            icon = Icons.Filled.PlayArrow,
            route = "carousel"
        )
    )
}