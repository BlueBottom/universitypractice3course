// AppModule.kt
package com.example.practice.di

import android.app.Application
import androidx.room.Room
import com.example.practice.database.AppDatabase
import com.example.practice.features.main.MainViewModel
import com.example.practice.features.main.RandomNumbersUseCase
import com.example.practice.features.main.domain.MainRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single {
        Room.databaseBuilder(
            get<Application>(),
            AppDatabase::class.java,
            "shorts_database"
        ).build()
    }

    single { get<AppDatabase>().shortsDao() }

    single { MainRepository(get()) }
    single { RandomNumbersUseCase() }

    viewModel { MainViewModel(get(), get()) }
}
