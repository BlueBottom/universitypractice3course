package com.example.practice.ui.main

import BottomNavigationBar
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.MailOutline
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.practice.ui.NavHostContainer
import com.example.practice.ui.common.TopBar

@Composable
fun MainScreen() {

    val navController = rememberNavController()

    val topBarState = rememberSaveable { (mutableStateOf(true)) }

    val navBackStackEntry by navController.currentBackStackEntryAsState()

    LaunchedEffect(navBackStackEntry) {
        val route = navBackStackEntry?.destination?.route
        topBarState.value = when {
            route == "carousel" -> true
            route == "shorts" -> false
            route?.startsWith("currentShort/") == true -> true
            else -> topBarState.value
        }
    }

    Surface(color = Color.White) {
        Scaffold(
            topBar = {
                TopBar(topBarState = topBarState)
            },
            bottomBar = {
                BottomNavigationBar(navController = navController)
            }, content = { padding ->
                NavHostContainer(navController = navController, padding = padding)
            }
        )
    }
}

@Composable
fun OptionsList() {
    val options = listOf(
        Pair("Описание", Icons.Default.Info),
        Pair("Добавить в плейлист", Icons.Default.Add),
        Pair("Субтитры", Icons.Default.Edit),
        Pair("Качество", Icons.Default.Settings),
        Pair("Не интересует", Icons.Default.Close),
        Pair("Не рекомендовать видео с этого канала", Icons.Default.Delete),
        Pair("Пожаловаться", Icons.Default.Warning),
        Pair("Отправить отзыв", Icons.Default.MailOutline),
    )

    LazyColumn(contentPadding = PaddingValues(bottom = 15.dp)) {
        items(options) { (option, icon) ->

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 20.dp)
                    .clickable { }
            ) {
                Icon(
                    imageVector = icon,
                    contentDescription = option,
                    modifier = Modifier.padding(end = 20.dp),
                    tint = Color(206, 204, 204, 255)
                )
                Text(
                    text = option,
                    style = TextStyle(
                        color = Color(206, 204, 204, 255),
                        fontSize = 16.sp)
                )
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
fun MainActivityPreview() {
    MainScreen()
}