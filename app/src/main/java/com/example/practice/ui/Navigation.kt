package com.example.practice.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.practice.features.main.MainViewModel
import com.example.practice.ui.main.CurrentShortScreen
import com.example.practice.ui.main.ShortsCarouselScreen
import com.example.practice.ui.main.ShortsListScreen
import org.koin.androidx.compose.koinViewModel


@Composable
fun NavHostContainer(
    viewModel: MainViewModel = koinViewModel(),
    navController: NavHostController,
    padding: PaddingValues
) {

    NavHost(
        navController = navController,
        startDestination = "shorts",
        modifier = Modifier.padding(paddingValues = padding)
    ) {

        composable("shorts") {
            ShortsListScreen(viewModel, navController)
        }

        composable("carousel") {
            ShortsCarouselScreen()
        }

        composable(
            route = "currentShort/{id}",
            arguments = listOf(
                navArgument("id") {
                    type = NavType.LongType
                }
            )
        ) { backStackEntry ->
            backStackEntry.arguments?.let { it ->
                val shortsModel by viewModel.getShortsById(it.getLong("id", 0)).observeAsState()

                shortsModel?.let { model ->
                    if (model.id != 0L) {
                        CurrentShortScreen(
                            shortsModel = model,
                            viewModel = viewModel
                        )
                    }
                }
            }
        }
    }
}
