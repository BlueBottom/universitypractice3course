package com.example.practice.ui.main

import androidx.annotation.OptIn
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.ui.PlayerView
import com.example.practice.database.ShortsModel
import com.example.practice.features.main.MainViewModel
import com.example.practice.ui.common.ReactionButtonView
import org.koin.androidx.compose.koinViewModel

@OptIn(UnstableApi::class)
@Composable
fun CurrentShortScreen(shortsModel: ShortsModel, viewModel: MainViewModel = koinViewModel()) {
    val context = LocalContext.current
    var videoUrl by remember { mutableStateOf<String?>(null) }
    val exoPlayer = remember(context) {
        ExoPlayer.Builder(context).build().apply {
            repeatMode = Player.REPEAT_MODE_ALL
        }
    }

    var shortsLikes by remember { mutableStateOf<Pair<Int, Int>>(Pair(0, 0)) }

    LaunchedEffect(shortsModel) {
        videoUrl = shortsModel.videoId
        videoUrl?.let {
            exoPlayer.setMediaItem(MediaItem.fromUri(shortsModel.videoId))
            exoPlayer.prepare()
            exoPlayer.playWhenReady = true
        }

        shortsLikes = Pair(
            viewModel.getRandomNumber(100, 3000),
            viewModel.getRandomNumber(10, 2000)
        )
    }

    DisposableEffect(Unit) {
        onDispose {
            exoPlayer.release()
        }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        var isLiked by remember { mutableStateOf<Boolean?>(null) }
        var isPlaying by remember { mutableStateOf(true) }

        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Black)
                .clickable {
                    if (exoPlayer.isPlaying) {
                        exoPlayer.pause()
                        isPlaying = false
                    } else {
                        exoPlayer.play()
                        isPlaying = true
                    }
                },
            contentAlignment = Alignment.Center
        ) {
            AndroidView(
                modifier = Modifier.fillMaxSize(),
                factory = { context ->
                    PlayerView(context).apply {
                        player = exoPlayer
                        useController = false
                    }
                }
            )

            if (!isPlaying) {
                Icon(
                    imageVector = Icons.Filled.PlayArrow,
                    contentDescription = "Play",
                    modifier = Modifier
                        .align(Alignment.Center)
                        .size(100.dp),
                    tint = Color(255, 255, 255, 160)

                )
            }

            Text(
                text = shortsModel.description,
                color = Color(255,255,255,225),
                modifier = Modifier
                    .align(Alignment.BottomStart)
                    .padding(16.dp)
            )

            Column(
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .padding(end = 10.dp)
            ) {
                Spacer(modifier = Modifier.height(265.dp))
                ReactionButtonView(
                    modifier = Modifier.align(Alignment.End),
                    text = "${shortsLikes.first}",
                    isDislike = false,
                    onClick = {
                        isLiked = true
                    },
                    isOptionSelected = isLiked
                )
                Spacer(modifier = Modifier.height(20.dp))
                ReactionButtonView(
                    modifier = Modifier.align(Alignment.End),
                    text = "${shortsLikes.second}",
                    isDislike = true,
                    onClick = {
                        isLiked = false
                    },
                    isOptionSelected = if (isLiked != null) !isLiked!! else null
                )
            }
        }
    }
}
