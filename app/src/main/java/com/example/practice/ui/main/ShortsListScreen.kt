package com.example.practice.ui.main

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.rememberLazyStaggeredGridState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.practice.features.main.MainViewModel
import com.example.practice.ui.common.CardView
import org.koin.androidx.compose.koinViewModel

@Composable
fun ShortsListScreen(viewModel: MainViewModel = koinViewModel(), navController: NavHostController) {
    val state = rememberLazyStaggeredGridState()
    val shortsList by viewModel.shortsList.observeAsState(emptyList())

    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Fixed(2),
        modifier = Modifier
            .background(Color(27, 27, 27, 255))
            .fillMaxSize()
            .padding(5.dp),
        state = state,
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalItemSpacing = 8.dp,
        content = {

            items(shortsList.size) {index ->
                CardView(
                    likes = viewModel.getRandomNumber(10, 2000),
                    dislikes = viewModel.getRandomNumber(10, 2000),
                    shortsList[index],
                    onClick = {
                        navController.navigate("currentShort/${shortsList[index].id}")
                    }
                )
            }
        }
    )
}