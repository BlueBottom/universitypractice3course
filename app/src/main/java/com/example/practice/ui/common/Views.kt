package com.example.practice.ui.common

import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.ImageLoader
import coil.compose.AsyncImage
import coil.decode.VideoFrameDecoder
import com.example.practice.R
import com.example.practice.database.ShortsModel

@Composable
fun CardView(likes: Int, dislikes: Int, shortsModel: ShortsModel, onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {

        val imageLoader = ImageLoader.Builder(LocalContext.current)
            .components { add(VideoFrameDecoder.Factory()) }
            .build()

        AsyncImage(
            model = Uri.parse(shortsModel.videoId),
            imageLoader = imageLoader,
            contentDescription = stringResource(id = R.string.image_content_description),
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxSize()
                .clickable { onClick() }
                .clip(RoundedCornerShape(6.dp))
        )
        Row(
            modifier = Modifier
                .align(Alignment.BottomStart)
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = R.drawable.like),
                contentDescription = stringResource(id = R.string.like),
                tint = Color.White,
                modifier = Modifier.size(24.dp)
            )
            Text(
                text = "$likes",
                color = Color.White,
                modifier = Modifier.padding(start = 4.dp)
            )
            Spacer(modifier = Modifier.width(16.dp))
            Icon(
                painter = painterResource(id = R.drawable.dislike),
                contentDescription = stringResource(id = R.string.dislike),
                tint = Color.White,
                modifier = Modifier.size(24.dp)
            )
            Text(
                text = "$dislikes", // Количество дизлайков
                color = Color.White,
                modifier = Modifier.padding(start = 4.dp)
            )
        }
    }
}

@Composable
fun ReactionButtonView(
    modifier: Modifier,
    onClick: (() -> Unit)? = null,
    isOptionSelected: Boolean?,
    text: String,
    isDislike: Boolean
) {
    val interactionSource by remember { mutableStateOf(MutableInteractionSource()) }
    Column(modifier = modifier) {

        Icon(
            modifier = Modifier
                .size(40.dp)
                .scale(scaleX = 1f, scaleY = if (isDislike) -1f else 1f)
                .clickable(
                    interactionSource = interactionSource,
                    indication = null,
                    onClick = {
                        if (onClick != null) {
                            onClick()
                        }
                    }),
            imageVector = Icons.Filled.ThumbUp,
            contentDescription = null,
            tint = if (isOptionSelected != null && isOptionSelected) Color(0xFF2196F3) else Color.White
        )

        Text(
            text = text, modifier = Modifier
                .align(Alignment.CenterHorizontally),
            style = TextStyle(color = Color.White)
        )
    }
}

@Preview
@Composable
fun ReactionButtonPreview() {
    ReactionButtonView(
        modifier = Modifier,
        text = "123",
        isDislike = false,
        isOptionSelected = true
    )
}