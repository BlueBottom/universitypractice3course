package com.example.practice.ui.common

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.practice.ui.main.OptionsList

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(topBarState: MutableState<Boolean>) {
    val title = "Shorts"
    var showBottomSheet by remember { mutableStateOf(false) }
    val sheetState = rememberModalBottomSheetState()
    val scope = rememberCoroutineScope()
    AnimatedVisibility(
        visible = topBarState.value
    ) {
        TopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(Color(44, 44, 44, 255)),
            title = { Text(text = title, color = Color(206, 204, 204, 255)) },
            modifier = Modifier
                .height(60.dp)
                .wrapContentHeight(align = Alignment.CenterVertically),
            actions = {
                IconButton(onClick = { showBottomSheet = true }) {
                    Icon(
                        imageVector = Icons.Filled.MoreVert,
                        contentDescription = "MoreVert",
                        tint = Color(206, 204, 204, 255)
                    )
                }
            }
        )
    }
    if (showBottomSheet) {
        ModalBottomSheet(
            containerColor = Color(44,44,44,255),

            onDismissRequest = {
                showBottomSheet = false
            },
            sheetState = sheetState,
            modifier = Modifier
                .padding(bottom = 20.dp),

            ) {
            Column(modifier = Modifier.navigationBarsPadding()) {
                OptionsList()
            }
        }
    }
}