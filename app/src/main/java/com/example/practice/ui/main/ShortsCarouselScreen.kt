package com.example.practice.ui.main

import androidx.annotation.OptIn
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.pager.VerticalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.ui.PlayerView
import com.example.practice.features.main.MainViewModel
import com.example.practice.ui.common.ReactionButtonView
import org.koin.androidx.compose.koinViewModel

@OptIn(UnstableApi::class)
@Preview(showBackground = true)
@Composable
fun ShortsCarouselScreen(viewModel: MainViewModel = koinViewModel()) {
    val pagerState = rememberPagerState(pageCount = { 10 })
    val context = LocalContext.current

    val shortsList by viewModel.shortsList.observeAsState(emptyList())
    var shortsLikes by remember { mutableStateOf<List<Pair<Int, Int>>>(emptyList())}

    LaunchedEffect(shortsList) {
        val list = mutableListOf<Pair<Int, Int>>()
        if (shortsList.isNotEmpty()) {
            shortsList.forEach {
                list.add(
                    Pair(
                        viewModel.getRandomNumber(100, 3000),
                        viewModel.getRandomNumber(10, 2000)
                    )
                )
            }
        }
        shortsLikes = list
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
    ) {
        VerticalPager(
            modifier = Modifier.fillMaxSize(),
            state = pagerState
        ) { page ->
            var isLiked by remember { mutableStateOf<Boolean?>(null) }
            var isPlaying by remember { mutableStateOf(true) }

            val videoUrl = shortsList.getOrNull(page)?.videoId
            val exoPlayer = remember(context) {
                ExoPlayer.Builder(context).build().apply {
                    repeatMode = Player.REPEAT_MODE_ALL
                }
            }

            DisposableEffect(Unit) {
                onDispose {
                    exoPlayer.release()
                }
            }

            LaunchedEffect(videoUrl, pagerState.currentPage) {
                if (pagerState.currentPage == page) {
                    exoPlayer.stop()
                    videoUrl?.let {
                        exoPlayer.apply {
                            setMediaItem(MediaItem.fromUri(it))
                        }
                        exoPlayer.prepare()
                        exoPlayer.playWhenReady = true
                    }
                }
            }

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .clickable {
                        if (exoPlayer.isPlaying) {
                            exoPlayer.pause()
                            isPlaying = false
                        } else {
                            exoPlayer.play()
                            isPlaying = true
                        }
                    },
                contentAlignment = Alignment.Center
            ) {
                AndroidView(
                    modifier = Modifier.fillMaxSize(),
                    factory = { context ->
                        PlayerView(context).apply {
                            player = exoPlayer
                            useController = false
                        }
                    }
                )

                if (!isPlaying) {
                    Icon(
                        imageVector = Icons.Filled.PlayArrow,
                        contentDescription = "Play",
                        modifier = Modifier
                            .align(Alignment.Center)
                            .size(100.dp),
                        tint = Color(255,255,255,160)
                    )
                }

                Text(
                    text = shortsList[page].description,
                    color = Color(255,255,255,225),
                    modifier = Modifier
                        .align(Alignment.BottomStart)
                        .padding(16.dp)
                )

                Column(
                    modifier = Modifier
                        .align(Alignment.CenterEnd)
                        .padding(end = 10.dp)
                ) {
                    Spacer(modifier = Modifier.height(265.dp))
                    ReactionButtonView(
                        modifier = Modifier.align(Alignment.End),
                        text = "${if (shortsLikes.isNotEmpty()) shortsLikes[page].first else 0}",
                        isDislike = false,
                        onClick = {
                            isLiked = true
                        },
                        isOptionSelected = isLiked
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ReactionButtonView(
                        modifier = Modifier.align(Alignment.End),
                        text = "${if (shortsLikes.isNotEmpty()) shortsLikes[page].second else 0}",
                        isDislike = true,
                        onClick = {
                            isLiked = false
                        },
                        isOptionSelected = if (isLiked != null) !isLiked!! else null
                    )
                }
            }
        }
    }
}
