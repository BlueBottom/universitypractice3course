package com.example.practice

import android.app.Application
import android.content.Intent
import com.example.practice.di.appModule
import com.example.practice.features.main.MainActivity
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class PracticeApplication: Application() {
    override fun onCreate(){
        super.onCreate()
        startKoin {
            androidContext(this@PracticeApplication)
            modules(appModule)
        }
        val intent = Intent(this@PracticeApplication, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}