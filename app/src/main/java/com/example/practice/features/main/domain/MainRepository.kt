package com.example.practice.features.main.domain

import androidx.lifecycle.LiveData
import com.example.practice.database.ShortsDao
import com.example.practice.database.ShortsModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

class MainRepository(private val shortsDao: ShortsDao) {
    private val coroutineScope = CoroutineScope(Dispatchers.Main)

    val shortsList: LiveData<List<ShortsModel>> = shortsDao.getShorts()

    fun getShortsByVideoId(videoId: String): ShortsModel? {
            return shortsDao.getShortsByVideoId(videoId)
    }

    fun getShortsById(id: Long): LiveData<ShortsModel> {
        val shortsModelLiveData = shortsDao.getShortsById(id)
        shortsModelLiveData.observeForever {
        }
        return shortsModelLiveData
    }

    fun insertShort(id:Long, videoId: String, decription: String) {
        val short = ShortsModel(
            id = id,
            videoId = videoId,
            description = decription
        )
        shortsDao.insertShort(short)
    }

    fun delete() {
        shortsDao.delete()
    }
}