package com.example.practice.features.main

class RandomNumbersUseCase {
    fun getRandomNumber(start: Int, end: Int) = (start..end).random()
}