package com.example.practice.features.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practice.database.ShortsModel
import com.example.practice.features.main.domain.MainRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(
    private val repository: MainRepository,
    private val randomNumbersUseCase: RandomNumbersUseCase
) : ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.delete()
            repository.insertShort(0,"https://storage.yandexcloud.net/video-storage-practice/Mama%20I’m%20criminal.mp4", "тутуту")
            repository.insertShort(1,"https://storage.yandexcloud.net/video-storage-practice/coffee.mp4", "аоыатцушат")
            repository.insertShort(2,"https://storage.yandexcloud.net/video-storage-practice/coffee_2.mp4", "Coffee")
            repository.insertShort(3,"https://storage.yandexcloud.net/video-storage-practice/inst-vs-reality.mp4", "Coffee_2")
            repository.insertShort(4,"https://storage.yandexcloud.net/video-storage-practice/picture.mp4", "Picture")
            repository.insertShort(5,"https://storage.yandexcloud.net/video-storage-practice/pust.mp4", "Pust")
            repository.insertShort(6,"https://storage.yandexcloud.net/video-storage-practice/skanti.mp4", "Skanti")
            repository.insertShort(7,"https://storage.yandexcloud.net/video-storage-practice/stone.mp4", "Stone")
            repository.insertShort(8,"https://storage.yandexcloud.net/video-storage-practice/truk.mp4", "Truk")
            repository.insertShort(9,"https://storage.yandexcloud.net/video-storage-practice/who_right.mp4", "Right")

        }
    }
    fun getRandomNumber(start: Int, end: Int) =
        randomNumbersUseCase.getRandomNumber(start, end)

    val shortsList: LiveData<List<ShortsModel>> = repository.shortsList

     fun getShortsByVideoId(videoId: String): ShortsModel {
         var result = ShortsModel(0, "", "")
         viewModelScope.launch(Dispatchers.IO) {
             result = repository.getShortsByVideoId(videoId) ?: ShortsModel(0, "", "")
         }

         return result
    }

    fun getShortsById(id: Long): LiveData<ShortsModel> = repository.getShortsById(id)

    fun insertShorts(id: Long, videoId: String, description: String) {
        repository.insertShort(id, videoId, description)
    }
}